#!/usr/bin/python3

"""
Generate consistent/combine metadata JSON file mapping a JPG as key
and a subset of metadata extract from iiif JSON file (if available) as a value.
"""

import os, sys, json, re
from os.path import basename

ALIAS_PREFIX_CODE = '_alias_:'

meta_keep = [
    # parismuseescollections.paris.fr
    "Thèmes / Sujets / Lieux représentés",
    "Description",
    "Commentaire historique",
    "Époque/Période",
    "Description iconographique",
    "Datation en siècle",
    "Matériaux et techniques",
    "Date de production",
    "Auteur",
    "Dimensions",
    "Institution",
    # artic.edu
    "Artist / Maker"
    "Collection"
    "Dimensions"
    "Medium"
]

# For artic.edu
artic_map = {}
def load_map_artic():
    global artic_map
    with open('image-list.txt', 'r') as f:
        lines = [l.strip().split("\t", 1) for l in f.readlines()]
        artic_map = {re.sub(r'.*/iiif/2/([0-9a-f-]+)/.*', r'\1', k): v for k, v in lines}

def get_filenames_artic_edu(l, j):
    urls = [image[0]['resource']["@id"] for image in (c['images'] for c in j['sequences'][0]['canvases'])]
    uuids = [re.sub(r'.*/iiif/2/([0-9a-f-]+)/.*', r'\1', u) for u in urls]
    return [artic_map[u] for u in uuids if u in artic_map]

# For parismuseescollections.paris.fr
def get_filenames_fr(l, j):
    urls = [image[0]['resource']["@id"] for image in (c['images'] for c in j['sequences'][0]['canvases'])]
    filenames = [re.sub(r'\?.*$', '', basename(u)) for u in urls]
    return filenames

def extract(l, func):
    with open(l.strip(), 'r') as f:
        try:
            j = json.load(f)
        except json.decoder.JSONDecodeError as e:
            print(l + str(e), file=sys.stderr)
            return
        filenames = func(l, j)
        iiif = j['@id']
        meta = {k['label']: k['value'] for k in filter(lambda s: s['label'] in meta_keep, j['metadata'])}
        first = True
        for fn in filenames:
            if first:
                data[fn] = {'iiif': iiif, 'label': j['label'], 'meta': meta}
                if 'description' in j:
                    if isinstance(j['description'], str) and j['description'] != "":
                        data[fn]['desc'] = j['description']
                    try:
                        if isinstance(j['description'], list) and j['description'][0]['value'] != "":
                            data[fn]['desc'] = j['description'][0]['value']
                    except KeyError:
                        pass
                first = False
            else:
                data[fn] = ALIAS_PREFIX_CODE + filenames[0]

data = {}

if True:
    load_map_artic()
    _func = "get_filenames_artic_edu"
else:
    _func = "get_filenames_fr"

for l in sys.stdin.readlines():
    extract(l, globals()[_func])
print(json.dumps(data))
