#!/usr/bin/awk -f

# Copyright (C) 2016 Raphaël Droz
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version

# Live stream Drupal 8 mysqldump replacing sensible data
# Ex: reset passwords to foo in the new dump:
# awk -v password=xxx strip-d8-dump.awk
# awk -v password=$(./core/scripts/password-hash.sh bla|tail -1|cut -f2) strip-d8-dump.awk

BEGIN {
	FS="[`,()]";
	ONE_WEEK = 3600*24*7;
	if (length(password) == 0) {
		l_password = "blah";
	} else {
		l_password = password;
	}
}

{
	if ($0 ~ /^INSERT INTO `[\w]*users_field_data`/) {
		n = split($0, a, /VALUES \(|\),\(/, seps);
		for(seg in a) {
			if (a[seg] !~ /@/) {
				printf(a[seg]);
				printf(seps[seg]);
				continue;
			}
			n2 = split(a[seg], fields, /,/);
			# replace 5th (login), 6th (pass), 7th (mail), 14th (init)
			fields[5] = "'uid" fields[1] "'";
			fields[6] = "'" l_password "'";
			fields[7] = fields[14] = "'uid+" fields[1] "@localhost.local'";
			# round / 10000 fields 10-13 (created, changed, access, login)
			for (i = 10; i <= 13; i++) {
				fields[i] = sprintf("%d", fields[i] / (ONE_WEEK)) * (ONE_WEEK);
			}
			for (i = 1; i <= n2; i++) {
				if(i > 1) printf(",");
				printf(fields[i])
			}
			printf(seps[seg]);
		}
	}
}

