#!/bin/bash

# Copyright (C) 2013, 2014 Raphaël Droz
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version


# Given such lines in /etc/hosts
# # fd00::1234 machine1
# # fd00::1235 machine2
# # fd00::1236 machine3
# will setup the IPv6 for this machine to a specified interface
# or otherwise from the interface having a local IPv4 already configured.


# First argument (optional): the interface name (wlan0/eth0)
# If not provided it'll use interface having a local IPv4 configured.

# no iface definition
if [[ -z "$1" ]]; then
    # interface having a local ipv4 adress (= "being up")
    iface=$(ip -o -4 addr show to 192.168/16|awk '{print $2}')
else
    iface=$1 && shift
fi

if ! type -P sudo &> /dev/null; then
    echo >&2 "no sudo, won't change netdev setup"
    exit 1
fi

if ! ip addr show dev $iface &> /dev/null; then
    echo >&2 "invalid iface $iface"
    exit 1
fi

addr=$(grep -wF "$HOSTNAME" /etc/hosts|grep "^fd00::"|awk '{print $1}')
if [[ -z "$addr" || ! $addr =~ ^fd00::[0-9a-f][0-9a-f][0-9a-f][0-9a-f]$ ]]; then
    echo >&2 "no address setup in /etc/hosts for HOSTNAME $HOSTNAME"
    exit 1
fi


del_iface=( $(ip -o -f inet6 address show|grep -F "$addr"|awk '{print $2}') )

if (( ${#del_iface[@]} > 0 )); then

    if (( ${#del_iface[@]} == 1 )) && [[ ${del_iface[0]} == $iface ]]; then
	echo >&2 "$iface inet already initialized, nothing to do"
	exit 0
    fi

    if [[ $@ =~ -f ]]; then
	echo >&2 "$addr already attributed to (${del_iface[@]}), removing"
	for del_iface in ${del_iface[@]//$iface/}; do
	    sudo ip addr del $addr/64 dev $del_iface
	done

	# $iface already bound to $addr (because inside del_iface, nothing more to do
	[[ $del_iface =~ $iface ]] && exit 0
	sudo ip addr add $addr/64 dev $iface
	ret=$?
	logger -s -t bin/inet6 -p user.info "ipv6 addr on $iface = $addr"
	exit $ret
    fi

    echo >&2 "$addr already attributed to (${del_iface[@]}), aborting"
    exit 1
fi


# ! ip -o -f inet6 address show dev $iface|grep -qF "$addr"
sudo ip addr add $addr/64 dev $iface
ret=$?
(( $ret != 0 )) && {
    echo 0|sudo tee /proc/sys/net/ipv6/conf/$iface/disable_ipv6 1>/dev/null
    sudo ip addr add $addr/64 dev $iface
    ret=$?
}
if (( $ret != 0 )); then
    logger -s -t bin/inet6 -p user.err "ipv6 addr failed for $iface = $addr"
else
    logger -s -t bin/inet6 -p user.info "ipv6 addr on $iface = $addr"
fi
exit $ret
