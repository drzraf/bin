#!/usr/bin/env python3
#
# Decompressor/compressor for files in Mozilla's "mozLz4" format. Firefox uses this file format to
# compress e. g. bookmark backups (*.jsonlz4).
#
# This file format is in fact just plain LZ4 data with a custom header (magic number [8 bytes] and
# uncompressed file size [4 bytes, little endian]).
#
# This Python 3 script requires the LZ4 bindings for Python, see: https://pypi.python.org/pypi/lz4
#
# Copyright (c) 2018, Raphaël Droz
# Copyright (c) 2015, Tilman Blumenbach
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions
#    and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials provided
#    with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, argparse
from lz4 import block

class InvalidHeader(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg

def decompress(file_obj):
    if file_obj.read(8) != b"mozLz40\0":
        raise InvalidHeader("Invalid magic number")
    return block.decompress(file_obj.read())

def compress(file_obj):
    compressed = block.compress(file_obj.read())
    return b"mozLz40\0" + compressed

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="MozLz4a compression/decompression utility")
    argparser.add_argument("-d", "--decompress", "--uncompress", action="store_true", help="Decompress the input file instead of compressing it.")
    argparser.add_argument("in_file", type=argparse.FileType('rb'), default=sys.stdout, help="Path to input file.")
    argparser.add_argument("-o", "--output", type=argparse.FileType('wb'), default=sys.stdout.buffer, help="Output file.")
    args = argparser.parse_args()

    try:
        data = decompress(args.in_file) if args.decompress else compress(args.in_file)
        args.output.write(data)
    except IOError as e:
        print("Could not write to output file `%s': %s" % (args.output.name, e), file=sys.stderr)
        sys.exit(5)
    except Exception as e:
        print("Could not %s file `%s': %s" % ('decompress' if args.decompress else 'compress', args.in_file.name, e), file=sys.stderr)
        sys.exit(4)
    finally:
        args.output.close()
