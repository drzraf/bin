#!/usr/bin/python3

import configparser, os, sys, re, argparse

# Menu of options within 'flags' field of target definition
# flag_opt = [
#     { "defaults",   0U,             FLG_DEFAULTS },
#     { "user",       ~0U,            FLG_USER },
#     { "nouser",     ~FLG_USER,      0U },
#     { "fsck",       ~0U,            FLG_FSCK },
#     { "nofsck",     ~FLG_FSCK,      0U },
#     { "mkswap",     ~0U,            FLG_MKSWAP },
#     { "nomkswap",   ~FLG_MKSWAP,    0U },
#     { "trim",       ~0U,            FLG_TRIM },
#     { "notrim",     ~FLG_TRIM,      0U } ]

# # Menu of options within 'bootaction' field of target definition
# boot_options = [
#     { "none",       ~FLG_BOOT_MASK,     0 },
#     { "mount",      ~FLG_BOOT_MASK,     FLG_BOOT_MOUNT },
#     { "swap",       ~FLG_BOOT_MASK,     FLG_BOOT_SWAP },
#     { "prepare",    ~FLG_BOOT_MASK,     FLG_BOOT_PREP } ]


# opt = [
#     { "flags",          OFFSET(flags), proc_flags,     setup_options },
#     { "bootaction",     OFFSET(flags), proc_flags,     boot_options },
#     { "dev",            OFFSET(dev), proc_string,    NULL },
#     { "dir",            OFFSET(dir), proc_string,    NULL },
#     { "startsector",    OFFSET(start), proc_int64,     NULL },
#     { "numsectors",     OFFSET(length), proc_int64,     NULL },
#     { "fstype",         OFFSET(fstype),proc_string,    NULL },
#     { "mountoptions",   OFFSET(mountoptions), proc_string,    NULL },
#     { "fsckoptions",    OFFSET(fsckoptions), proc_string,    NULL },
#     { "loop",           OFFSET(loopdev), proc_string,    NULL },
#     { "cipher",         OFFSET(cipher), proc_string,    NULL },
#     { "ivoffset",       OFFSET(ivoffset), proc_int64,     NULL },
#     { "keyformat",      OFFSET(key.format), proc_string,    NULL },
#     { "keyfile",        OFFSET(key.filename), proc_string,    NULL },
#     { "keyhash",        OFFSET(key.digestalg), proc_string,    NULL },
#     { "keycipher",      OFFSET(key.cipheralg), proc_string,    NULL },
#     { "keymaxlen",      OFFSET(key.maxlen), proc_long,      NULL },
#     { "passwdretries",  OFFSET(key.retries), proc_unsigned,  NULL } ]

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument('-m', '--only-mount', help='only print the mount command', action="store_true")
group.add_argument('-c', '--only-crypt', help='only print the cryptsetup command', action="store_true")
group.add_argument('-a', '--only-autofs', help='only print an autofs program: compatible string', action="store_true")
# http://bugs.python.org/issue19959
# parser.add_argument('--config-file', type=argparse.FileType('r', expanduser=True), help='configuration file to use (default: %(default)s)', default="~/ucmtab.ini", metavar='config')
parser.add_argument('--config-file', type=argparse.FileType('r'), help='configuration file to use (default: %(default)s)', default=os.path.expanduser("~/ucmtab.ini"), metavar='config')
parser.add_argument('secname', metavar="section-name")
args = parser.parse_args()

config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation(),
                                   default_section = "default")
config.read(args.config_file.name)

# config['default'] = {'User': os.getusername()}
# print(config.sections())

secname = args.secname
if secname not in config:
    print("error: section \"%s\" does not exist in %s" % ( secname, args.config_file.name), file=sys.stderr)
    sys.exit(1)
section = config[secname]
# print(section)

dm = {
    "fstype": None,
    "keyformat": None,
    "keyfile": "-",
    "dev": None,
    "dir": None,
    "mountoptions": None,
    "autofs": None,
    "flags": None
}

# will automagically take [default] values into account and override them from "section"
for key in section:
    for i in dm.keys():
        if i in section: dm[i] = section[i]
#print(dm)

if not args.only_autofs and (args.only_crypt or not args.only_mount):
    print("cryptsetup open --type %(device_type)s --key-file %(keyfile)s %(device)s %(name)s%(separator)s"
       %
        {
        'device_type': dm['keyformat'],
        'keyfile': dm['keyfile'],
        'device': os.path.expanduser(dm['dev']),
        'name': secname,
        'separator': " && \\" if not args.only_mount and not args.only_crypt else ""
        })

if not args.only_autofs and (args.only_mount or not args.only_crypt):
    print("mount --fstab ~/.fstab %(mountpoint)s" % { 'mountpoint': os.path.expanduser(dm['dir']) });

if args.only_autofs:
    print("%(autofs)s%(keyfile)s" % { 'autofs': dm['autofs'], 'keyfile': ",keyfile=" + os.path.expanduser(dm['keyfile']) if dm['keyfile'] else "" })

'''
# ~/.ucmtab - encrypted filesystem information for ucryptmount
# make the link between users' encrypted block devices and ~/.fstab
# long-run solution:
# mount.crypt -o crypto_name=blah ~/file.bin mntpoint 
[default]
fstype=ext4
keyformat=luks
kdir=~/keys

[db]
dev=~/db.bin
dir=~/db
keyfile=${kdir}/db
mountoptions=defaults,user,noauto
autofs=-fstype=crypt,crypto_name=db,keyfile=%{keyfile},fsk_cipher=none
'''
