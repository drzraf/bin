#!/usr/bin/python3

import sys
from datetime import date
from time import strptime
import argparse

# ordered list of astrological signs
zodiacs = [(120, 'Capricorn'), (218, 'Aquarius'), (320, 'Pisces'), (420, 'Aries'), (521, 'Taurus'),
           (621, 'Gemini'), (722, 'Cancer'), (823, 'Leo'), (923, 'Virgo'), (1023, 'Libra'),
           (1122, 'Scorpio'), (1222, 'Saggitarius'), (1231, 'Capricorn')]
ordered = {i[1]: k for k, i in enumerate(zodiacs[0:12])}

def sunsign12(birthdate):
    date_number = int("".join((str(birthdate.month), '%02d' % birthdate.day)))
    for z in zodiacs:
        if date_number <= z[0]:
            return z[1], ordered[z[1]] + 1

def parse():
    parser = argparse.ArgumentParser(description="date to zodiac sign")
    parser.add_argument('-n', '--numbered', action="store_true", help='Prefix output with the yearly order, starting at 0')
    parser.add_argument('-a', '--approx', action="store_true", help='tilda-prefixed output for in-between birthdays')
    parser.add_argument('-d', '--debug', action="store_true", help='Debug')
    parser.add_argument('month_day_1', help='Month or day')
    parser.add_argument('month_day_2', help='Month or day')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse()
    if args.month_day_1[0].isalpha():
        month = strptime(args.month_day_1,'%b').tm_mon
        day = args.month_day_2
    elif args.month_day_2[0].isalpha():
        month = strptime(args.month_day_2,'%b').tm_mon
        day = args.month_day_1
    else:
        month = args.month_day_1
        day = args.month_day_2

    bdate = date.fromisoformat("2000-{0:02d}-{1:02d}".format(int(month), int(day)))
    if args.debug:
        print(f"Birthday: {bdate}", file=sys.stderr)

    sign, num = sunsign12(bdate)
    if args.approx and bdate.day in [19,20,21,22,23]:
        sign = '~' + sign
    if args.numbered:
        print(f"{num:02d} {sign}")
    else:
        print(sign)
