#!/bin/bash
# to be placed in /etc/acpi/actions/lid_toggle.sh
# + sudoers authorization
conf=/etc/default/acpi-support


! grep -q '^LID_SLEEP=' $conf && sed -i "/^#* * LID_SLEEP/s/^#* *//" $conf

function lid_suspend_status() {
    value=nothing
    cur=$(sed -n '/^LID_SLEEP=/{s/^.*=//p;q}' $conf)
    [[ -n "$cur" && "$cur" = true ]] && value=suspend
    echo $value
}

cur=$(lid_suspend_status)
word=


if [[ -z "$1" || "$1" = status ]]; then
	echo $cur
	exit 0
fi


if [ "$1" = toggle ]; then
    [ $cur = nothing ] && word=true
    [ $cur = suspend ] && word=false
elif [[ "$1" = suspend ]]; then
    [[ $cur != suspend ]] && word=true
elif [ "$1" = nothing ]; then
    [[ $cur = suspend ]] && word=false
else
    exit 1
fi
if [[ -n "$word" ]]; then
    sed -i "/^LID_SLEEP=/s/=.*/=$word/" $conf
    echo $(lid_suspend_status)
else
    echo $cur
fi
