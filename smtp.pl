#!/usr/bin/perl -w
use Net::SMTP;
use Net::SMTP::SSL;

if ($#ARGV < 4 || $#ARGV > 6) {
    print "$0 server from to subject message [ssl] [ssl port]\n";
    exit 1;
}

$smtpserver = "$ARGV[0]";
$from = "$ARGV[1]";
$to = "$ARGV[2]";
$subject = "$ARGV[3]";
$message = "$ARGV[4]\n";
#print "$smtpserver $from $to $subject $message";

# Envoi du message
if ($#ARGV > 4 && $ARGV[5] eq "ssl") {
    $port=465;
    if ($#ARGV == 6) { $port=$ARGV[6];}
    $smtp = Net::SMTP::SSL->new("$smtpserver",
				Timeout => 20,
				Port => $port);
}
else {
    $smtp = Net::SMTP->new("$smtpserver",
			   Timeout => 20,
			   Port => 25);
}

$smtp->mail("$from" );
$smtp->to("$to" );
$smtp->data();
$smtp->datasend("To: $to\n" );
$smtp->datasend("From: $from\n" );
$smtp->datasend("X-Mailer: Perl Sendmail \n" );
$smtp->datasend("Subject: $subject\n" );
$smtp->datasend("\n" );
$smtp->datasend("$message\n" );
$smtp->dataend();
$smtp->quit();

exit 0;
