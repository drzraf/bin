#!/bin/bash

# warning: file pattern matching in /var/lib/apt/lists/ should always
# go equal or more specific as the check is deeper
function __check_integrity_debian_repositories() {
    # cf __check_integrity_debian_repositories
    rdebsums apt debian-archive-keyring lsb-release || return 1
    # cf __check_integrity_debian_packages_file
    rdebsums coreutils grep || return 1
    # cf __check_integrity_debian_package()
    rdebsums sed sha1sum || return 1 # coreutils : tac
    # cf __check_integrity_debian_package_md5sums
    rdebsums md5sum dpkg bash || return 1
    # TODO: use /usr/share/keyrings (chmod'ed +r)
    [[ -r /etc/apt/trusted.gpg ]] || return 1
    apt-key adv -q --no-verbose --verify-files /var/lib/apt/lists/*InRelease &>/dev/null
}

# related: askubuntu.com/questions/253728/
function __check_integrity_debian_packages_file() {
    # s'assurer de la validité de ces fichiers de Packages
    # (= leur présence dans les *InRelease correspondants)
    # Note: |grep -qf - /var/lib/apt/lists/*jessie*InRelease # retournerait aussi 0 pour une unique match
    # (cf git-grep --all)
    local sums
    sums=$(sha256sum /var/lib/apt/lists/*$(lsb_release -cs)*Packages|cut -d' ' -f1)
    for i in $sums; do
	grep $i /var/lib/apt/lists/*$(lsb_release -cs)*InRelease || return 1
    done
    return 0
}

function __check_integrity_debian_package() {
    # extract deb package basename and sha1sum on 2 distinct lines
    # read -rs -d"\n" filename sha1sum < <(
    # 	sed -rn "/Package: $1\$/,+20{/^(Filename|SHA1):/s;.*[ /];;p}" \
    # 	    /var/lib/apt/lists/*$(lsb_release -cs)*Packages
    # )

    # generate output suitable for direct sha1sum --check, eg:
    # 11452524db14339cbc439e3c64e8bb836f2454aa /var/cache/apt/archives/apt-transport-https_1.0.9.8_i386.deb
    sed -rn "/Package: $1\$/,+20{/^(Filename|SHA1):/s;.*[ /];;p}" \
	/var/lib/apt/lists/*$(lsb_release -cs)*Packages \
	|sed -re 'N;s/\n/ /;s;(.*) (.*);\2 /var/cache/apt/archives/\1;'|sha1sum --status --check
    return $?
}

# ensure the md5sums /var/lib/dpkg/info/*.md5sums (which are the base trusted by debsums)
# were not spoofed
function __check_integrity_debian_package_md5sums() {
    local dir expected_md5sum current_mds filename
    dir=$(mktemp -d /tmp/XXXXXX)
    filename=$(dpkg-query -W -f='/var/cache/apt/archives/${Package}_${Version}_${Architecture}.deb\n' "$1")
    dpkg-deb -e "$filename" $dir && \
	read -er -d' ' expected_md5sum < <(md5sum $dir/md5sums)
    read -rs -d' ' current_mds < <(md5sum "/var/lib/dpkg/info/$1.md5sums")
    return [[ $current_mds == $expected_md5sum ]]
}

function __check_integrity_debian() {
    if $(type -P rdebsums &>/dev/null); then
	if dpkg --compare-versions 2.1 '<=' $(debsums --version|sed -n '1s/.* //p'); then
	    rdebsums -s debsums cryptmount cryptsetup libpam-mount linux-image-686-pae &>/dev/null
	    return $?
	fi
	# rdebsums return value is always 0
	rdebsums debsums cryptmount mount cryptsetup|grep -q FAILED && return 1
	return 0;
    fi

    files="$(ldd /usr/bin/cryptmount /bin/mount|grep -ow '/[^: ]*'|sort -u|xargs -n1 readlink -f|sort -u)"
    pkg=$(dpkg -S $files|awk -F: '{print $1}'|sort -u)

    if $(type -P debsums &>/dev/null); then
	debsums $pkg
	return;
    fi

    sums=$(for i in $pkg; do cat /var/lib/dpkg/info/$i*.md5sums; done;)
    red_sums=$(grep -f <(sed 's;^/;;'<<<"$files")<<<"$sums")
    pushd /
    echo "$red_sums"|md5sum --quiet -c -
    #echo "$red_sums"|md5sum --status -c -
    popd
}

function __check_integrity_rpm() {
    rpm -V `rpm -qf /bin/ps`
}

# bootstrap script mount an encrypted loop device
# where user files are (including *bashrc* & co)
function __env_bootstrap() {
    local dir="$1" && shift
    [[ -z $dir || ! -d $dir ]] && return 1

    # TODO
    __y_mount() {
	! grep -q "$dir" /etc/fstab && \
	    ! mount|grep -q "$dir" && \
	    mount $dir && \
	    return 0
    }

    __y_symlink() {
	! type -P xstow &> /dev/null && return 1
	xstow -dir ~/.datas -target ~ dotfiles
    }

    __y_symlink_old() {
	# printf "%s:%s\n" mount2 $(date +%X:%N) >> /tmp/logit #debug
	if [[ -n $ENV_SOURCE_LINK_POLICY ]]; then
	    dircombine2 -v -l $ENV_SOURCE_LINK_POLICY -f $ENV_SOURCE/.homelinks $HOME $dir
	else
	    dircombine2 -v -f $dir/.homelinks $HOME $dir 
	fi
	#TODO: if we want them:
	#dircombine2 -nvd $HOME $dir/misc-apps/
    }

    ! type -P cryptmount &> /dev/null && return 1
    ! cryptmount -l|grep -q -- myData && return 1
    mount|grep -q -- /dev/mapper/myData && return 0

    key=$__ENV_PDIR/.keys/datas
    [[ ! -r $key ]] && { mount $__ENV_PDIR || return 1; }
    cryptmount -w 5 myData 5<$key
}

function __env_mount_private() {
    sudo cryptsetup isLuks "$(findmnt -nls -o SOURCE -T /)"
    #  sudo dmsetup ls --tree|sed -n "/$(findmnt -nls -o SOURCE -T /|grep -Po '\K[^/]*$')/,/:5/p"|tail -2|head -1
    let is_root_crypted=$?
    sudo cryptsetup isLuks "$(findmnt -nls -o SOURCE -T /home)"
    let is_home_crypted=$?
    { ps faux; tty; } > /tmp/zoblog.txt
    if (( $is_home_crypted == 0 && $is_root_crypted == 0 )); then
	__check_integrity_debian;
	ok=$?
    else
	ok=0
    fi
    if (( $ok == 0 )); then
	if [[ -n $DISPLAY ]]; then xterm -geometry 200x50+0+0 -e /bin/bash -c "sudo service cryptdisks start";
	else sudo service cryptdisks start;
	fi
	mount $__ENV_PDIR
    fi
}

if ! findmnt -nls -o TARGET -T $__ENV_PDIR|grep -qwf - <(/bin/mount -l); then
    __env_mount_private &
fi
