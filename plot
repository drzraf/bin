#!/bin/bash
TF='%d/%b/%Y:%H:%M:%S'
COLS=${1:-1:2}
gnuplot -p -e "set xdata time; set timefmt '$TF'; set format x '%Y-%m'; set yrange [0:60000]; set xtics rotate; plot '-' using $COLS with lines;"
