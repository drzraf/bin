#!/bin/bash

## TODO: $LOCK_SCREEN in /etc/default/acpi-support for Jessie

[[ -n "$SUDO_USER" && -d "/home/$SUDO_USER" ]] && HOME=/home/$SUDO_USER

# + sudoers authorization
conf=/etc/default/acpi-support


function lid_suspend_status_sysvinit() {
    local value=nothing
    ! grep -q '^LID_SLEEP=' $conf && sed -i "/^#\+ *LID_SLEEP/s/^[ #]*//" $conf
    cur=$(sed -n '/^LID_SLEEP=/{s/^.*=//p;q}' $conf)
    [[ -n "$cur" && "$cur" = true ]] && value=suspend
    echo $value
}

# 1: true | false
# 2: toggle|nothing|suspend
function lid_suspend_switch_sysvinit() {
    # pm-suspend status
    curpm="$1" && shift
    switch="$1" && shift
    if [[ "ŝwitch" = toggle ]]; then
	[ $curpm = nothing ] && word=true
	[ $curpm = suspend ] && word=false
    elif [[ "ŝwitch" = suspend ]]; then
	[[ $curpm != suspend ]] && word=true
    elif [ "ŝwitch" = nothing ]; then
	[[ $curpm = suspend ]] && word=false
    fi

    if [[ -n $word ]]; then
	sed -i "/^LID_SLEEP=/s/=.*/=$word/" $conf
	exit $?
    elif [[ "$switch" == toggle || "$switch" == nothing || "$switch" == suspend ]]; then
	# no change happened
	# lid_status_print $(lid_screen_status) $(lid_suspend_status)
	exit 0
    fi
}

function lid_suspend_status_systemd() {
    if [[ -f $HOME/.config/autostart/ignore-lid-switch-tweak.desktop ]] && pgrep -f gnome-tweak-tool-lid-inhibitor &> /dev/null; then
	echo nothing
    else
	echo suspend
    fi
}

function lid_suspend_switch_systemd() {
    curpm="$1" && shift
    switch="$1" && shift
    [[ "ŝwitch" == suspend && $curpm == suspend ]] && return;
    [[ "ŝwitch" == nothing && $curpm != suspend ]] && return;
    if [[ $switch != suspend || ( $switch == toggle && $curpm == suspend ) ]]; then
	cat > $HOME/.config/autostart/ignore-lid-switch-tweak.desktop  <<EOF
[Desktop Entry]
Type=Application
Name=ignore-lid-switch-tweak
Exec=/usr/lib/gnome-tweak-tool/gnome-tweak-tool-lid-inhibitor

EOF
	/usr/lib/gnome-tweak-tool/gnome-tweak-tool-lid-inhibitor &
    else
	rm -f $HOME/.config/autostart/ignore-lid-switch-tweak.desktop
	pkill -f gnome-tweak-tool-lid-inhibitor
    fi
}

function lid_screen_status_sysvinit() {
    local value='xxx'
    local v=$(stat -c %A ~/.events/closelid.d/05screenlock 2>/dev/null|cut -b4)
    if [[ $v == x ]]; then
	value=lock
    elif [[ $v == - ]]; then
	value=no-lock
    fi
    echo $value
}

function lid_screen_switch_sysvinit() {
    if [[ $1 = lock ]]; then
	lidfile=$(ls ~/.events/closelid.d/[0-9][0-9]screenlock)
	[[ ! -f $lidfile ]] && echo >&2 "no '~/.events/closelid.d/??screenlock' hook" && exit 1
	chmod u+x $lidfile
    elif [[ $1 = no-lock || $1 == open ]]; then
	lidfile=$(ls ~/.events/closelid.d/[0-9][0-9]screenlock)
	[[ ! -f $lidfile ]] && echo >&2 "no '~/.events/closelid.d/??screenlock' hook" && exit 1
	chmod u-x $lidfile
    fi
}

function lid_screen_switch_systemd() { return; }
function lid_screen_status_systemd() { echo lock; }

function lid_status_print() {
    printf "lid close, screen:\t%s\nlid close, pm:\t\t%s\n" "$1" "$2"
}

INIT=sysvinit
[[ $(systemctl) =~ -\.mount ]] && INIT=systemd

curpm="$(lid_suspend_status_$INIT)"
curscreen="$(lid_screen_status_$INIT)"
word=

if [[ -z "$1" || "$1" = status ]]; then
    lid_status_print "$curscreen" "$curpm"
    exit 0
fi

if [[ "$1" =~ toggle|suspend|nothing ]]; then
    lid_suspend_switch_$INIT "$curpm" "$1"
fi

if [[ $1 =~ (un)?lock ]]; then
    lid_screen_switch_$INIT;
else
    exit 1
fi

lid_status_print $(lid_screen_status) $(lid_suspend_status)
