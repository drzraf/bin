#!/bin/bash
#set -x
if [[ -z "$DISPLAY" ]]; then
    SCREEN=$(pgrep -f -l "^X.*$USER"|sed -n 's/^[0-9]* X \(:.\).*/\1/p')
    export DISPLAY=${SCREEN:-:0}
fi

BOX=
[ -n "$1" ] && BOX="($1)"
DZENICONPATH=~/.fluxbox/dzen
CONTENT_LINES=5

[ -e ${DZENICONPATH}/envelope.xbm ] && PMAIL="^fg(grey60)^i(${DZENICONPATH}/envelope.xbm)"

mapfile -t -n 60
let i=0
while [[ $i -lt 60 ]]; do
    if [[ ${MAPFILE[$i]} =~ ^From: ]]; then
	from=${MAPFILE[$i]/From:/}
    elif [[ ${MAPFILE[$i]} =~ ^Subject: ]]; then
	subject=${MAPFILE[$i]/Subject:/};
    elif [[ -z "${MAPFILE[$i]}" ]]; then
	((i++))
	let j=0;
	while [[ $j -lt $CONTENT_LINES ]]; do
	    [[ -n "${MAPFILE[$((i+j))]}" ]] && content+="${MAPFILE[$((i+j))]}\n"
	    ((j++))
	done
	break;
    fi
    ((i++))
done
{
    echo " "$PMAIL "^fg(yellow)"$from"^p(30)^fg(blue)"$BOX
    echo "^fg(red)"${subject^^}
    echo -ne "$content"
}|dzen2 -l $((CONTENT_LINES)) -sa l -ta l -fn '-misc-dejavu serif-bold-r-normal--12-0-0-0-p-*-iso8859-15' -bg '#2c2c32' -fg 'grey70' -p -x 700 -w 580 2>&1
