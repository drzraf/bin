#!/bin/bash
# Copyright (C) 2013, Raphaël Droz

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# see ucrypt/ucryptmount for explanations

[[ $(basename $0) == umount ]] && {
    echo >&2 "luks umount: TODO"
    /bin/umount "$@"
    exit $?
}

# wrap mount calls applying on known mount points of luks-encrypted device
# in order to pass dynamically an adequete key

# TODO: ensure we support fstab using both models:
# /dev/mapper/xxx	mount-point
# block-encrypted.luks	mount-point

[[ ! -f ~/.fstab && ! -d ~/.fstab.d ]] && {
    /bin/mount "$@"
    exit $?
}

# [[ -z "$1" || $1 =~ -[lhV] ]] && {
#     /bin/mount "$@"
#     exit $?
# }

# local lastarg mntp
lastarg="${@: -1}"
# \K ? http://unix.stackexchange.com/questions/13466
devicemapper_name=$(findmnt -F ~/.fstab.d/ -nfs -o OPTIONS "$lastarg"|grep -Po 'crypto_name=\K\w+')
[[ -z $devicemapper_name ]] && {
    /bin/mount "$@"
    exit $?
}

[[ -z $KEYDIR || ! -d $KEYDIR ]] && echo >&2 "\$KEYDIR undefined" && exit 1
KDIR="$KEYDIR"

if [[ ! -f $KDIR/$devicemapper_name ]]; then
    # one could just provide any file on stdin
    /bin/mount "$@"
    exit;
fi


# return crypto device open-count and live-status
# (we try to avoid double-luksOpening)
mounted=$(sudo dmsetup info --noheadings -c -o open,attr --separator="" $devicemapper_name 2>/dev/null)

# decrypted but not mount:
if [[ ${mounted:0:2} == 0L ]]; then
    sudo mount /dev/mapper/$devicemapper_name "$lastarg"
    exit $?
fi
# TODO: if returns 1L or 2L, device is already mounted... somewhere else?


# < 2.21 doesn't support --fstab
if [[ $(/bin/mount -V|grep -Eo -m1 '[0-9.]+'|head -1) =~ 2.(1\d|20) ]]; then
    if command grep -qF "$(realpath $lastarg)" /etc/fstab; then
        echo >&2 "... aliased mount < 2.21"
        sudo mount "$@" < $KDIR/$devicemapper_name
    else
        /bin/mount "$@"
    fi
elif [[ -d ~/.fstab.d ]] && command grep -qF "$(realpath $lastarg)" ~/.fstab.d/*.fstab &>/dev/null; then
    echo >&2 "... aliased mount (~/.fstab.d)"
    sudo mount "$@" --fstab ~/.fstab.d < $KDIR/$devicemapper_name
elif command grep -qF "$(realpath $lastarg)" ~/.fstab &>/dev/null; then
    echo >&2 "... aliased mount (~/.fstab)"
    sudo mount "$@" --fstab ~/.fstab < $KDIR/$devicemapper_name
else
    /bin/mount "$@"
fi


## Recall:
# $ /bin/mount .mp < .keys/mp
# will trigger
# $ /sbin/mount.crypto_LUKS /dev/loop5 ~/.mp -o allopts,keyfile=/dev/stdin,crypto_name=mp < .keys/mp
# will need:
# open("/dev/loop5", O_RDONLY)
# open("/dev/mapper/mp", O_RDONLY)
# open("/dev/mapper/control", O_RDWR|O_LARGEFILE)
# The later will fail unless kernel-level permission access on the mapper/loop control device
# is implemented in order to open it (eg: chmod o+w /dev/mapper/control)

# Last obstacle is
# > mount: only root can use "--fstab" option
