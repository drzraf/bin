#!/bin/bash

[[ -z $1 || -z $2 ]] && exit

case $1 in
    json)
	json_pp -t dumper -json_opt utf8,pretty < "$2"
	;;
    ffbksqlite)
	#sqlite3 "$2" "SELECT id, title FROM moz_bookmarks;"|sort
	#sqlite3 "$2" "SELECT a.id, a.url, b.title FROM moz_places a LEFT JOIN moz_bookmarks b ON (a.id = b.fk);"|sort
	sqlite3 "$2" "SELECT a.url, b.title FROM moz_places a LEFT JOIN moz_bookmarks b ON (a.id = b.fk);"|sort
	#sqlite3 "$2" "SELECT url FROM moz_places;"|sort
	# note:
	# compare URL only stripping protocol
	# git diff dd*/places.sqlite|grep -- '^+'|awk -F '|' '{print $2}'|sed -E 's;http(s)?://;;'
	;;
    sqlite)
	sqlite3 "$2" ".dump"
	;;
    ffprefs)
	grep -Ev '\b(app.update|datareporting.sessions|extensions.bootstrappedAddons|extensions.enabledAddons|extensions.installCache|extensions.adblockplus.notificationdata|idle.lastDailyNotification|extensions.requestpolicy.allowedOriginsToDestinations|extensions.requestpolicy.allowedDestinations|pdfjs.database|places.database.lastMaintenance|places.history.expiration.transient_current_max_pages|services.sync.bookmarks.last|services.sync.clients.last|services.sync.last|toolkit.startup.last|browser.uiCustomization.state|devtools.telemetry.tools.opened.version)' "$2"
	;;
esac
