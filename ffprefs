#!/bin/bash

let update=0

DNS=127.0.0.1

# See
# https://github.com/brave/brave-core/blob/master/components/query_filter/utils.cc#27
declare -a url_tracking_parameters=(
    __hsfp
    __hssc
    __hstc
    __s
    _branch_match_id
    _branch_referrer
    _fbp
    _gcl_au
    _gl
    _hsenc
    _kx
    _openstat
    at_recipient_id
    at_recipient_list
    bbeml
    bsft_clkid
    bsft_uid
    ceid
    dclid
    drip_email
    drip_subscriber_id
    emci
    emdi
    et_rid
    fb_action_ids
    fb_comment_id
    fbclid
    gbraid
    gclid
    guce_referrer
    guce_referrer_sig
    hootPostID
    hsCtaTracking
    igshid
    irclickid
    mc_eid
    mid
    mkt_tok
    ml_subscriber
    ml_subscriber_hash
    mmdr
    msclkid
    mtm_cid
    oft_c
    oft_ck
    oft_d
    oft_id
    oft_ids
    oft_k
    oft_lk
    oft_sk
    oly_anon_id
    oly_enc_id
    pk_cid
    rb_clickid
    s_cid
    sourceid
    ss_email_id
    twclid
    unicorn_click_id
    utm_content
    utm_id
    utm_term
    vero_conv
    vero_id
    vgo_ee
    wbraid
    wickedid
    yclid
    ymclid
    ysclid
)

declare -A privacy_prefs=(
    [dom.private-attribution.submission.enabled]=false
    [browser.safebrowsing.downloads.remote.enabled]=false
    [accessibility.blockautorefresh]=true
    [app.normandy.enabled]=false
    [app.normandy.first_run]=false
    [browser.safebrowsing.downloads.enabled]=false
    [browser.safebrowsing.downloads.remote.enabled]=false
    [browser.safebrowsing.malware.enabled]=false
    [browser.safebrowsing.phishing.enabled]=false
    [browser.search.suggest.enabled]=false
    [browser.tabs.crashReporting.sendReport]=false
    [browser.urlbar.suggest.topsites]=false
    [datareporting.healthreport.uploadEnabled]=false
    [experiments.activeExperiment]=false
    [extensions.blocklist.enabled]=false
    [extensions.getAddons.cache.enabled]=false
    [network.connectivity-service.enabled]=false
    [network.captive-portal-service.enabled]=false
    [network.dns.disablePrefetch]=true
    [network.dns.disablePrefetchFromHTTPS]=true
    [network.ftp.enabled]=true
    [network.http.speculative-parallel-limit]=0
    [network.trr.custom_uri]=$DNS
    [network.trr.uri]=$DNS
    [privacy.query_stripping.enabled]=true
    [security.OCSP.enabled]=0
    [toolkit.telemetry.reportingpolicy.firstRun]=false
    [privacy.query_stripping.strip_list]=${url_tracking_parameters[@]}
)

declare -A dev_prefs=(
    [security.fileuri.strict_origin_policy]=false
)

declare -A extra_prefs=(
    [browser.eme.ui.enabled]=false
    [media.eme.enabled]=false
)

function is_string() {
    [[ $1 =~ ^(true|false|[0-9]+)$ ]] && return 1 || return 0
}

tmp=$(declare -p privacy_prefs)
eval "${tmp/privacy_prefs/prefs}"

[[ $1 == -u ]] && update=1 && shift
file="$1" && shift

if [[ -z $file ]]; then
    for i in ${!prefs[*]}; do
	echo "$i=${prefs[$i]}"
    done
    exit
fi

for key in ${!prefs[*]}; do
    req_val="${prefs[$key]}"
    line=$(grep "user_pref(\"$key\"," "$file");
    old_val=$(sed "s/.*\"$key\", \"\?//;s/\"\?);$//"<<<"$line");
    if [[ $old_val != $req_val ]]; then
	echo -e "$key\t \"$value\" != \"$req_val\""
	if (( update )); then
	    encoded="$req_val"
 	    is_string "$req_val" && encoded="\"${encoded//\"/\\\"}\""
	    if [[ -z $old_val ]]; then
		printf 'user_pref("%s", %s);\n' "$key" "$encoded" | tee -a "$file"
	    else
		sed -ri '/user_pref\("'"$key"'",/s/", .*/", '"$encoded"');/' "$file"
	    fi
	fi
    fi
done
