#!/bin/bash
# Overlay mp3 file(s) with a record (of name of the song)
# Don't forget: /etc/pulse/default.pa
# load-module module-echo-cancel aec_args="analog_gain_control=0 digital_gain_control=1"
# to get clean songname recording

VOLUME=4
FILTER="[1:0]volume=$VOLUME[b],[0][b]amix=inputs=2:duration=longest"
FILTER1="[1:0]volume=$VOLUME[b],[0][b]amix=inputs=2:duration=shortest"

[[ -z "$1" ]] && exit 1

if [[ -d "$1" ]]; then
    dst=($1/*.mp3)
else
    dst=("$@")
fi

m="([ -]?)(minor|major|flat|sharp)"
script=$(cat <<EOF
/A$m/I{s/A$m/LA \2/i; bx}
/B$m/I{s/B$m/SI \2/i; bx}
/C$m/I{s/C$m/DO \2/i; bx}
/D$m/I{s/D$m/RE \2/i; bx}
/E$m/I{s/E$m/MI \2/i; bx}
/F$m/I{s/F$m/FA \2/i; bx}
/G$m/I{s/G$m/SOL \2/i; bx}
:x
s/flat/bemol/i
s/sharp/dièse/i
EOF
)

for i in "${dst[@]}"; do
    speechfile="$(dirname "$i")/speech/$(basename "${i/.mp3/.wav}")"
    r=$(basename "$speechfile" | sed -re "$script")
    [[ -s "$speechfile" ]] && continue

    mkdir -p "$(dirname "$speechfile")"
    echo "Record title of $speechfile... // $r"
    read a
    destfile="$(dirname "$i")/mp3/$(basename "$i")"
    rm -f "$destfile"
    sleep 0.4 && echo "Recording..." && parecord -r >| "$speechfile"
done

echo "creating segments"
for i in "${dst[@]}"; do
    speechfile=$(dirname "$i")/speech/$(basename "${i/.mp3/.wav}")
    segmentfile=$(dirname "$i")/speech/$(basename "${i/.mp3/.overlay.mp3}")
    [[ -s "$speechfile" && -s "$segmentfile" ]] && continue

    ffmpeg -i "$i" -i "$speechfile" -filter_complex "$FILTER1" -aq 2 "$segmentfile"
done


echo "concating"
for i in "${dst[@]}"; do
    segmentfile=$(dirname "$i")/speech/$(basename "${i/.mp3/.overlay.mp3}")
    destfile=$(dirname "$i")/mp3/$(basename "$i")
    [[ -s "$segmentfile" && -s "$destfile" ]] && continue
    duration=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$segmentfile")

    ## concat filter reencode everything and does not work with adelay
    # CONCAT_FILTER="[1] adelay=$duration [orig] ; [0] [orig] concat=n=2:v=0:a=1"

    ## concat filter reencode everything
    # CONCAT_FILTER="[0] [1] concat=n=2:v=0:a=1"
    # ffmpeg -i "$segmentfile" -ss "$duration" -i "$i" -filter_complex "$CONCAT_FILTER" "$destfile"

    ## concat protocol works
    # duration=$(bc<<<"$duration + 0.0")
    ffmpeg -f concat -safe 0 -i <(echo -e "file '$(realpath "$segmentfile")'\nfile '$(realpath "$i")'\ninpoint $duration") -c copy "$destfile"
done
