#!/bin/bash
#set -x
#socat -u UDP-RECVFROM:10000,fork EXEC:/tmp/mail_notif.sh

read f;
afile=${f/*:/};

[ ! -r $afile ] && exit
if [[ -z "$DISPLAY" ]]; then
    SCREEN=$(pgrep -f -l "^X.*$USER"|sed -n 's/^[0-9]* X \(:.\).*/\1/p')
    export DISPLAY=${SCREEN:-:0}
fi

BOX="($(basename $(dirname $(dirname $afile))))"
DZENICONPATH=~/.fluxbox/dzen
CONTENT_LINES=5
DADIR=/tmp/.mail_notification

[ -r ${DZENICONPATH}/envelope.xbm ] && PMAIL="^fg(grey60)^i(${DZENICONPATH}/envelope.xbm)"

## possible: PHP quoted_printable_decode()
## or 822field from net-mail/mess822
subject=$(822field subject < $afile)
from=$(822field from < $afile)
ripmime --overwrite -q -i $afile -d $DADIR
[ -r $DADIR/textfile0 ] && content="$(grep -v '^$' $DADIR/textfile0|grep -v '^> *$'|head -$CONTENT_LINES)"

{
    echo " "$PMAIL "^fg(yellow)"$from"^p(30)^fg(blue)"$BOX
    echo "^fg(red)"${subject^^}
    echo -ne "$content"
}|dzen2 -l $((CONTENT_LINES)) -sa l -ta l -fn '-misc-dejavu serif-bold-r-normal--12-0-0-0-p-*-iso8859-15' -bg '#2c2c32' -fg 'grey70' -p -x 700 -w 580 2>&1
