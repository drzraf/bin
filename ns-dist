#!/bin/bash

declare -a failed
declare -a NAMESPACES=($NAMESPACES)
declare -a EXCLUDED_NAMESPACES=(${EXCLUDED_NAMESPACES})
EXCLUDED_NAMESPACES+=(fr protected)

if (( ${#NAMESPACES[@]} == 0 )); then
    NAMESPACES=($(systemctl -t service --no-legend --no-pager | awk '{if ($0 ~ /nsopenvpn@.*loaded active/) print($NF);}'))
fi

for del in "${EXCLUDED_NAMESPACES[@]}"; do
    NAMESPACES=(${NAMESPACES[@]%$del})
    if (( ${#NAMESPACES[@]} == 0 )); then
	echo >&2 "failed ns: No acceptable namespace left (${#EXCLUDED_NAMESPACES[@]} excluded)"
	exit 1
    fi
done

# Use a randomly chosen namespace (possibly proxified) to execute a command
let with_unnamespaced=0 reset=0 verbose=0
while getopts "fR:uUv" opt; do
    case $opt in
	f) fallback=1 ;;
	u) with_unnamespaced=1 ;;
	U) with_unnamespaced=0 ;;
	R) reset=1; RESOLVER="$OPTARG" ;;
	v) ((verbose++)) ;;
    esac
done
shift $((OPTIND-1))

[[ -z $1 ]] && echo >&2 "No command passed" && exit 1

(( with_unnamespaced && ! reset )) && NAMESPACES+=(__no_namespace__)
declare -a _NAMESPACES=(${NAMESPACES[@]})

function _vpnize() {
    ns="$1" && shift
    if [[ $1 == __no_namespace__ ]]; then
	"$@"
	ret=$?
    elif type -P vpnize &>/dev/null; then
	sudo -E NETNS="$ns" $(which vpnize) -x "$@"
	ret=$?
    else
	sudo ip netns exec "$ns" sudo -u $USER -i -- "$@"
	ret=$?
    fi
    return $ret
}

function _exit() {
    (( verbose && ${#failed[@]} > 0 )) && echo >&2 "failed ns: ${failed[@]}"
    (( verbose )) && echo >&2 "# used ns: $ns"
    exit $1
}

if (( reset )); then
    for ns in ${NAMESPACES[@]}; do
	echo "Was:"
	sudo ip netns exec $ns cat /etc/resolv.conf|sed 's/^/# /'
	echo "Set to:"
	echo nameserver "$RESOLVER" | sudo ip netns exec $ns tee /etc/resolv.conf
    done
elif (( ${#NAMESPACES[@]} > 0 )); then
    while (( ${#NAMESPACES[@]} > 0 )); do
	let r=$RANDOM
	ns="${NAMESPACES[$r%${#NAMESPACES[@]}]}"
	(( verbose > 1 )) && echo >&2 "# ns-dist using $ns ($r)"
	_vpnize "$ns" "$@"
	ret=$?
	(( ! fallback || ret == 0 )) && _exit $ret
	failed+=($ns)
	NAMESPACES=(${NAMESPACES[@]%$ns})
    done
    echo >&2 "failed: All ${#_NAMESPACES[@]} namespaces failed"
    exit 1
else
    (( verbose )) && echo >&2 "# ns-dist NOT namespaced"
    "$@"
fi
